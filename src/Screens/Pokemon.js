import React, { Component } from "react";
import axios from "axios";
import PokemonPost from "../Components/PokemonPost";
import { Col, Row } from "reactstrap";

export default class Pokemon extends Component {
  constructor() {
    super();
    this.state = {
      data: [],
      // limit: 10,
      offset: 1,
      favorits: [],
    };
  }
  componentDidMount() {
    axios
      .get(`https://pokeapi.co/api/v2/pokemon/?limit=12`)
      .then((response) => {
        this.setState({ data: response.data.results });
      });
  }

  render() {
    const { data } = this.state;
    console.log(this.state.data);

    const listPokemon = data.map((item, index) => (
      <Col>
        <PokemonPost
          key={index}
          name={item.name}
          img={`https://pokeres.bastionbot.org/images/pokemon/${index + 1}.png`}
        />
      </Col>
    ));

    return (
      <div>
        <Row style={{ backgroundColor: "#f8f1f1" }}>{listPokemon}</Row>
      </div>
    );
  }
}
