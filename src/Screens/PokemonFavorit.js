import React, { Component, Fragment } from "react";
import {
  Card,
  CardImg,
  CardText,
  CardBody,
  CardTitle,
  CardSubtitle,
  Button,
} from "reactstrap";
export default class PokemonFavorit extends Component {
  constructor() {
    super();
    this.state = {
      pokemon: [],
      favorits: [],
      limit: 10,
      offset: 0,
      isLoading: true,
    };
  }
  componentDidMount() {
    this.fetchData();
  }

  fetchData = () => {
    const { limit, offset } = this.state;
    this.setState({ isLoading: true });
    fetch(`https://pokeapi.co/api/v2/pokemon?limit=${limit}&offset=${offset}`)
      .then((responses) => responses.json())
      .then((data) => this.setState({ pokemon: data, isLoading: false }))
      .catch((error) => console.log(error));
  };

  render() {
    const { pokemon, isLoading, favorits } = this.state;

    return (
      <Fragment>
        <div>Pokemon Favorite</div>
        {favorits.length === 0 ? (
          <div>nothing Favorite</div>
        ) : (
          <div>
            {favorits.map((favorit, index) => (
              <div key={index}>
                <div>{favorit.name}</div>
              </div>
            ))}
          </div>
        )}
        <div>Pokemon List</div>
        <div>
          {pokemon.length === 0 || isLoading === true ? (
            <div>Loading...</div>
          ) : (
            pokemon.results.map((result, index) => (
              <Card style={{ width: "200px", margin: "1rem", display: "flex" }}>
                <CardBody>
                  <CardImg
                    src={`https://pokeres.bastionbot.org/images/pokemon/${
                      index + 1
                    }.png`}
                    alt="pokemon img"
                  ></CardImg>
                  <CardTitle tag="h3" className="text-center">
                    {result.name}
                  </CardTitle>
                  <Button
                    color="danger"
                    onClick={() =>
                      this.setState({
                        favorits: favorits.concat({
                          id: index,
                          name: result.name,
                        }),
                      })
                    }
                  >
                    {" "}
                    <i class="fas fa-heart"></i>
                  </Button>
                </CardBody>
              </Card>
            ))
          )}
        </div>
      </Fragment>
    );
  }
}
