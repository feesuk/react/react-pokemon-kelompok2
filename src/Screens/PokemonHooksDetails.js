import React, { useState, useEffect } from "react";
import { Button, Card, CardBody, CardImg, Row, Col } from "reactstrap";

import axios from "axios";

export default function PokemonHooksDetails(props) {
  const [data, setData] = useState([]);
  // const [favPokemon, setFavPokemon] = useState([]);
  const [loading, setLoading] = useState(true);

  // componentDidMount
  useEffect(() => {
    axios
      .get("https://pokeapi.co/api/v2/pokemon/?limit=100")
      .then((response) => {
        setData(response.data.results);
        setLoading(false);
      });

    console.log("Mounted!");

    return () => {
      console.log("Unmounted");
    };
  }, []);

  const getDetails = (id) => {
    console.log(id);
    props.history.push(`/pokemon-hooks/${id}`);
  };

  return (
    <div>
      <Row style={{ backgroundColor: "#f8f1f1" }}>
        {loading ? (
          <Col className="loading">
            <h1>Loading...</h1>
          </Col>
        ) : (
          data.map((item, index) => (
            <Col className="d-flex justify-content-center ">
              <Card className="m-2" style={{ width: "200px" }}>
                <CardBody>
                  <div key={index}>
                    <CardImg
                      top
                      width="100%"
                      // style={{ width: "180px" }}
                      src={`https://pokeres.bastionbot.org/images/pokemon/${
                        index + 1
                      }.png`}
                      alt={item.name}
                    ></CardImg>
                  </div>
                </CardBody>
                <CardBody>
                  <Button
                    outline
                    color="primary"
                    block
                    onClick={() => getDetails(index + 1)}
                  >
                    {item.name}
                  </Button>
                </CardBody>
              </Card>
            </Col>
          ))
        )}
      </Row>
    </div>
  );
}
