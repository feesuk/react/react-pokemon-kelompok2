import React from "react";
import BlogPost from "../Components/BlogPost";
import { Col, Row } from "reactstrap";

export default function Blog() {
  const posts = [
    {
      img:
        "https://i.pinimg.com/originals/bf/82/f6/bf82f6956a32819af48c2572243e8286.jpg",
      title: "Judul 1",
      content: ` Berita 1 : Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s,`,
    },
    {
      img:
        "https://i.pinimg.com/originals/91/b5/cd/91b5cdab51e207263169904b227503b4.jpg",
      title: "Judul 2",
      content: ` Berita 2 : Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s,`,
    },
    {
      img:
        "https://cloud.netlifyusercontent.com/assets/344dbf88-fdf9-42bb-adb4-46f01eedd629/60137856-c945-4d2e-8a78-45f91efcf157/nature-wallpaper10.jpg",
      title: "Judul 3",
      content: ` Berita 3 : Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s,`,
    },
    {
      img:
        "https://i.pinimg.com/originals/bf/82/f6/bf82f6956a32819af48c2572243e8286.jpg",
      title: "Judul 1",
      content: ` Berita 1 : Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s,`,
    },
    {
      img:
        "https://i.pinimg.com/originals/91/b5/cd/91b5cdab51e207263169904b227503b4.jpg",
      title: "Judul 2",
      content: ` Berita 2 : Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s,`,
    },
    {
      img:
        "https://cloud.netlifyusercontent.com/assets/344dbf88-fdf9-42bb-adb4-46f01eedd629/60137856-c945-4d2e-8a78-45f91efcf157/nature-wallpaper10.jpg",
      title: "Judul 3",
      content: ` Berita 3 : Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s,`,
    },
    {
      img:
        "https://i.pinimg.com/originals/bf/82/f6/bf82f6956a32819af48c2572243e8286.jpg",
      title: "Judul 1",
      content: ` Berita 1 : Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s,`,
    },
    {
      img:
        "https://i.pinimg.com/originals/91/b5/cd/91b5cdab51e207263169904b227503b4.jpg",
      title: "Judul 2",
      content: ` Berita 2 : Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s,`,
    },
    {
      img:
        "https://cloud.netlifyusercontent.com/assets/344dbf88-fdf9-42bb-adb4-46f01eedd629/60137856-c945-4d2e-8a78-45f91efcf157/nature-wallpaper10.jpg",
      title: "Judul 3",
      content: ` Berita 3 : Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s,`,
    },
  ];
  const postComponents = posts.map((post) => (
    <Col>
      <BlogPost img={post.img} title={post.title} content={post.content} />
    </Col>
  ));

  return (
    <div style={{ backgroundColor: "#f8f1f1" }}>
      <h1>Blog</h1>
      <Row>{postComponents}</Row>
    </div>
  );
}
