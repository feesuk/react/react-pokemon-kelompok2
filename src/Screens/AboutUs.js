import React, { useState } from "react";

export default function AboutUs() {
  const [name, setName] = useState("");
  const [message, setMessage] = useState("");
  const [developers, setDevelopers] = useState([
    {
      id: 1,
      name: "Wahyu",
    },
    {
      id: 2,
      name: "Ramadhan",
    },
  ]);
  const [newDev, setNewDev] = useState("");

  const handleSubmit = (event) => {
    event.preventDefault();
    setDevelopers([
      ...developers,
      {
        id: developers.length + 1,
        name: newDev,
      },
    ]);
    setNewDev("");
  };

  return (
    <div>
      <div>
        <div>Nama</div>
        <input
          type="text"
          value={name}
          onChange={({ target: { value } }) => setName(value)}
        />{" "}
        Nama saya adalah: {name}
        <div></div>
        <div>Message</div>
        <input
          type="text"
          value={message}
          onChange={({ target: { value } }) => setMessage(value)}
        />{" "}
        pesannya adalah : {message}
      </div>
      <div>---------------------------------------</div>
      <form onSubmit={handleSubmit}>
        <input
          value={newDev}
          onChange={({ target: { value } }) => setNewDev(value)}
        />
        <button type="submit">tambah</button>
      </form>
      <div>Developers</div>
      {developers.map((developers) => (
        <div>{developers.name}</div>
      ))}
    </div>
  );
}
