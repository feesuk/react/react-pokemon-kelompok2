import React, { useState, useEffect } from "react";
import axios from "axios";
import { Row, Col } from "reactstrap";

import SearchPokemon from "../Components/SearchPokemon";
import PokemonCard from "../Components/PokemonCard";

export default function PokemonHooksSearch(props) {
  const [data, setData] = useState([]);

  const [loading, setLoading] = useState(true);

  // componentDidMount
  useEffect(() => {
    axios
      .get("https://pokeapi.co/api/v2/pokemon/?limit=100")
      .then((response) => {
        let newResponse = response.data.results.map((item, index) => {
          return {
            name: item.name,
            url: item.url,
            id: index + 1,
            sprites: `https://pokeres.bastionbot.org/images/pokemon/${
              index + 1
            }.png`,
          };
        });
        console.log(newResponse);
        setData(newResponse);
        setLoading(false);
      });
  }, []);

  const getDetails = (id) => {
    console.log(id);
    props.history.push(`/pokemon-hooks/${id}`);
  };

  return (
    <div>
      {/* <Row> */}
      <Col style={{ backgroundColor: "#f8f1f1" }}>
        <SearchPokemon getDetails={getDetails} data={data} />
      </Col>

      <div>
        {loading ? (
          <Col className="loading" style={{ backgroundColor: "#f8f1f1" }}>
            <h1>Loading...</h1>
          </Col>
        ) : (
          data.map((item, index) => (
            <Row>
              <PokemonCard
                key={index}
                name={item.name}
                sprites={item.sprites}
                id={item.id}
                getDetails={getDetails}
              />
            </Row>
          ))
        )}
      </div>
      {/* </Row> */}
    </div>
  );
}
