import React, { Component } from "react";
import { Button, Form, FormGroup, Label, Input } from "reactstrap";

export default class ContactUs extends Component {
  constructor() {
    super();
    this.state = {
      message: "",
    };
  }
  render() {
    return (
      <div>
        <div className="content-form">
          <Form>
            <FormGroup>
              <Label for="exampleEmail">Email</Label>
              <Input
                type="email"
                name="email"
                id="exampleEmail"
                placeholder="with a placeholder"
              />
            </FormGroup>
            <FormGroup>
              <Label for="exampleText">Text Area</Label>
              <Input type="textarea" name="text" id="exampleText" />
            </FormGroup>
            <Button color="primary">Submit</Button>
          </Form>
        </div>
      </div>
    );
  }
}
