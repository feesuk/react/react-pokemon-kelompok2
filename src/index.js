import React from "react";
import ReactDOM from "react-dom";
import "./index.css";
import App from "./App";
import "bootstrap/dist/css/bootstrap.min.css";
import { createStore } from "redux";
import { Provider } from "react-redux";

// initial state
const globalState = {
  favoritePokemon: [],
};

// reducer
const rootReducer = (state = globalState, action) => {
  switch (action.type) {
    case "SET_FAVORITE_POKEMON":
      return {
        ...state,
        favoritePokemon: state.favoritePokemon.concat(action.newValue),
      };
    default:
      break;
  }
  return state;
};

//store
const storeRedux = createStore(rootReducer);

ReactDOM.render(
  <React.Fragment>
    <Provider store={storeRedux}>
      <App />
    </Provider>
  </React.Fragment>,
  document.getElementById("root")
);
