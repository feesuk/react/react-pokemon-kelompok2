import {
  Card,
  CardImg,
  CardBody,
  CardTitle,
  Button,
  Col,
  Row,
} from "reactstrap";
import React from "react";

export default function PokemonPost({ name, img }) {
  return (
    <div>
      <Row>
        <Col className="d-flex justify-content-center">
          <Card className="m-2" style={{ width: "200px" }}>
            <CardBody>
              <CardTitle style={{ width: "20px" }}>
                <img
                  src="https://www.flaticon.com/svg/vstatic/svg/914/914726.svg?token=exp=1610956528~hmac=bf40da2d61040cb31704356f227dd5b9"
                  alt="pokeball"
                ></img>
              </CardTitle>
              <CardImg top width="100%" src={img} className="p-2"></CardImg>
              <Button outline color="primary" block>
                {name}
              </Button>
            </CardBody>
          </Card>
        </Col>
      </Row>
    </div>
  );
}
