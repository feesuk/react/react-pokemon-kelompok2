import React, { useState } from "react";
import {
  Col,
  Row,
  Collapse,
  Navbar,
  NavbarToggler,
  NavbarBrand,
  Nav,
  NavItem,
  NavLink,
  UncontrolledDropdown,
  DropdownToggle,
  DropdownMenu,
  DropdownItem,
  NavbarText,
} from "reactstrap";

const NavbarStyled = (props) => {
  const [isOpen, setIsOpen] = useState(false);

  const toggle = () => setIsOpen(!isOpen);

  return (
    <Row>
      <Col>
        <Navbar color="primary" light expand="md">
          <NavbarBrand className="text-white" href="/">
            <img
              src="https://upload.wikimedia.org/wikipedia/commons/9/98/International_Pok%C3%A9mon_logo.svg"
              width="100px"
              alt="logo-pokemon"
            ></img>
          </NavbarBrand>
          <NavbarToggler onClick={toggle} />
          <Collapse isOpen={isOpen} navbar>
            <Nav className="mr-auto" navbar>
              <NavItem>
                <NavLink className="text-white" href="/">
                  <i className="fas fa-home"></i> Home
                </NavLink>
              </NavItem>
              <NavItem>
                <NavLink className="text-white" href="/profile">
                  <i className="fas fa-user"></i> Profile
                </NavLink>
              </NavItem>
              <NavItem>
                <NavLink className="text-white" href="/blog">
                  <i className="fas fa-book-open"></i> Blog
                </NavLink>
              </NavItem>

              <UncontrolledDropdown nav inNavbar color="primary">
                <DropdownToggle nav caret className="text-white">
                  <i className="fas fa-dot-circle"></i> Pokémon
                </DropdownToggle>
                <DropdownMenu right>
                  <DropdownItem>
                    <NavLink href="/pokemon">
                      <i className="fas fa-dot-circle"></i> Pokémon Regular
                    </NavLink>
                  </DropdownItem>
                  <DropdownItem>
                    <NavLink href="/pokemon-hooks-details">
                      <i className="fas fa-dot-circle"></i> Pokémon Hooks
                      Details
                    </NavLink>
                  </DropdownItem>
                  <DropdownItem>
                    <NavLink href="/pokemon-hooks-search">
                      <i className="fas fa-dot-circle"></i> Pokémon Hooks Search
                    </NavLink>
                  </DropdownItem>
                  <DropdownItem>
                    <NavLink href="/pokemon-fav">
                      <i className="fas fa-dot-circle"></i> Pokémon Fav
                    </NavLink>
                  </DropdownItem>
                </DropdownMenu>
              </UncontrolledDropdown>
              <NavItem>
                <NavLink className="text-white" href="/contact-us">
                  <i className="fas fa-envelope"></i> Contact Us
                </NavLink>
              </NavItem>
            </Nav>
          </Collapse>
        </Navbar>
      </Col>
    </Row>
  );
};

export default NavbarStyled;
