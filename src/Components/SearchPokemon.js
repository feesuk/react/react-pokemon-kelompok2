import React, { useState } from "react";
import { InputGroup, InputGroupAddon, Button, Input } from "reactstrap";

import PokemonCard from "./PokemonCard";

const SearchPokemon = (props) => {
  const [search, setSearch] = useState("");
  const [searchResult, setSearchResult] = useState("");

  const handleSearch = () => {
    let searchResult = props.data.find((item) => item.name === search);

    if (searchResult) {
      setSearchResult(searchResult);
    } else {
      setSearchResult("");
    }
    setSearch("");
  };

  return (
    <div>
      <div style={{ width: "300px", padding: "1.5rem 0" }}>
        <InputGroup>
          <Input
            onChange={(e) => setSearch(e.target.value)}
            // type="text"
            value={search}
          />
          <InputGroupAddon addonType="append">
            <Button color="primary" onClick={handleSearch}>
              Search
            </Button>
          </InputGroupAddon>
        </InputGroup>
      </div>
      {searchResult !== "" ? (
        <PokemonCard
          name={searchResult.name}
          sprites={searchResult.sprites}
          id={searchResult.id}
          getDetails={props.getDetails}
        />
      ) : (
        <div>Pokemon Not Found</div>
      )}
    </div>
    // <div>
    //   <div>
    //     <input
    //       className=""
    //       onChange={(e) => setSearch(e.target.value)}
    //       type="text"
    //       value={search}
    //     />
    //     <button onClick={handleSearch}>Search Pokemon</button>
    //   </div>

    // </div>
  );
};

export default SearchPokemon;
