import React from "react";
import {
  Card,
  Row,
  Col,
  CardImg,
  CardTitle,
  Button,
  CardBody,
} from "reactstrap";

const PokemonCard = (props) => {
  return (
    <div>
      {/* <Row style={{ backgroundColor: "#f8f1f1" }}> */}
      {/* <Col> */}
      <Card className="m-2" style={{ width: "200px" }}>
        <CardBody>
          <CardImg src={props.sprites} alt={props.name} />
          <CardTitle>{props.name}</CardTitle>
          <Button
            outline
            color="primary"
            block
            onClick={() => props.getDetails(props.id)}
          >
            Details
          </Button>
        </CardBody>
      </Card>
      {/* </Col> */}
      {/* </Row> */}
    </div>
  );
};

export default PokemonCard;
