import React, { useState, useEffect } from "react";
import { CardTitle, Card, CardBody, CardImg, Row, Col } from "reactstrap";
import axios from "axios";

export default function PokemonDetailsPage(props) {
  const [data, setData] = useState(null);

  useEffect(() => {
    const pokemonId = props.match.params.id;

    axios
      .get(`https://pokeapi.co/api/v2/pokemon/${pokemonId}`)
      .then((response) => {
        setData(response.data);
        console.log(response.data);
      })
      .catch((error) => console.log(error));

    return () => {
      setData(null);
    };
  }, []);

  return (
    // <div>
    //   {data === null ? null : (
    //     <div>
    //       <img src={data.sprites.front_default} alt={data.name} />
    //       <h2>{data.name}</h2>
    //     </div>
    //   )}
    // </div>

    <Row style={{ backgroundColor: "#f8f1f1" }}>
      {data === null ? null : (
        <Col body className="d-flex justify-content-center text-center loading">
          <Card>
            <CardBody>
              <CardImg
                top
                width="100%"
                // style={{ width: "180px" }}
                src={data.sprites.front_default}
                alt={data.name}
              ></CardImg>
              <CardTitle tag="h1">{data.name}</CardTitle>
            </CardBody>
          </Card>
        </Col>
      )}
    </Row>
  );
}
