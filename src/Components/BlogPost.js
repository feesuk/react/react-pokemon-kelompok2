import React from "react";
import {
  Card,
  CardImg,
  CardTitle,
  CardText,
  CardBody,
  Col,
  Row,
} from "reactstrap";

export default function BlogPost({ img, title, content }) {
  return (
    <div>
      <Row style={{ backgroundColor: "#f8f1f1" }}>
        <Col className="d-flex justify-content-center">
          <Card className="m-3">
            <CardImg top width="50px" src={img} alt="Card image cap" />
            <CardBody>
              <CardTitle tag="h5">{title}</CardTitle>
              <CardText>{content}</CardText>
            </CardBody>
          </Card>
        </Col>
      </Row>
    </div>
  );
}
