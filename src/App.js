import { BrowserRouter as Router, Route, Switch } from "react-router-dom";

import Home from "./Screens/Home";
import Profile from "./Screens/Profile";
import Blog from "./Screens/Blog";
import Pokemon from "./Screens/Pokemon";
import PokemonFavorit from "./Screens/PokemonFavorit";
import ContactUs from "./Screens/ContactUs";
import NavbarStyled from "./Components/Navbar";
import PokemonDetailsPage from "./Components/PokemonDetailsPage";
import PokemonHooksDetails from "./Screens/PokemonHooksDetails";
import PokemonHooksSearch from "./Screens/PokemonHooksSearch";
import AboutUs from "./Screens/AboutUs";

function App() {
  return (
    <Router>
      <NavbarStyled />

      <Switch>
        <Route component={Home} path="/" exact={true} />
        <Route component={Profile} path="/profile" />
        <Route component={Blog} path="/blog" />
        <Route component={Pokemon} path="/pokemon" />
        <Route component={PokemonFavorit} path="/pokemon-fav" />

        <Route
          component={PokemonHooksDetails}
          exact
          path="/pokemon-hooks-details"
        />
        <Route component={PokemonHooksSearch} path="/pokemon-hooks-search" />
        <Route component={PokemonDetailsPage} path="/pokemon-hooks/:id" />

        <Route component={ContactUs} path="/contact-us" />
        <Route component={AboutUs} path="/about-us" />
      </Switch>
    </Router>
  );
}

export default App;
