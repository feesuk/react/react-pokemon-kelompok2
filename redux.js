const redux = require("redux");
const createStore = redux.createStore;

const intialState = {
  favorite: 0,
  bestPokemon: "",
};

const rootReducer = (state = intialState, action) => {
  switch (action.type) {
    case "ADD_FAVORITE":
      return {
        ...state,

        favorite: state.favorite + 1,
        bestPokemon: action.valueBestPokemon,
      };

    // case "BEST_POKEMON":
    //   return {
    //     ...state,
    //     bestPokemon: action.valueBestPokemon,
    //   };
    // default:
    //   break;
  }
  return state;
};

const store = createStore(rootReducer);
// console.log(store.getState());

store.subscribe(() => {
  console.log(store.getState());
});

store.dispatch({
  //   type: "BEST_POKEMON",
  type: "ADD_FAVORITE",
  valueBestPokemon: "Boboho",
});

store.dispatch({
  //   type: "BEST_POKEMON",
  type: "ADD_FAVORITE",
  valueBestPokemon: "Boboho",
});

store.dispatch({
  //   type: "BEST_POKEMON",
  type: "ADD_FAVORITE",
  valueBestPokemon: "Boboho",
});
store.dispatch({
  //   type: "BEST_POKEMON",
  type: "ADD_FAVORITE",
  valueBestPokemon: "Raichu",
});

// store.dispatch({ type: "ADD_FAVORITE" });

// store.dispatch({ type: "BEST_POKEMON", valueBestPokemon: "Raichu" });

// console.log(store.getState());
// console.log(store.getState());
// store.dispatch({ type: "ADD_FAVORITE" });
// console.log(store.getState());
// store.dispatch({ type: "ADD_FAVORITE" });
// console.log(store.getState());
